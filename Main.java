package pl.sda;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        /*

        Zadanie 3.6
Telefon
Twój telefon dzwoni. Napisz metodę czyOdebrac zwracającą wartość true - jeżeli powinieneś odebrać,
false - w przeciwnym wypadku. Zastosuj następujące zasady. Zazwyczaj odbierasz telefon z wyjątkiem,
gdy jest rano. Wtedy odbierasz telefon tylko od Mamy. Jeżeli jednak jesteś śpiący nie odbierasz telefonu.
Parametry są podawane w kolejności: liczba danych wejściowych, następnie w każdej linii:
czyJestRano, czyDzwoniMama, czyJestemSpiacy.

Przykład:

Dane wejściowe:
4
false, false, false
false, false, true
true, false, false
true, true, false

Dane wyjściowe
true
false
false
true
         */


        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz liczbę zapytań:");

        int iloscZapytan = sc.nextInt();
        sc.nextLine();

        System.out.println("Wpisz zapytania " + "(" + iloscZapytan + ")" + " każde w oddzielnej linii:");


        String[] tablicaZapytan = new String[iloscZapytan];


        for (int i = 0; i < tablicaZapytan.length; i++) {

            tablicaZapytan[i] = sc.nextLine();

        }


        String[][] tablicaTablic = new String[iloscZapytan][3];

        boolean czyJestRano;
        boolean czyDzwoniMama;
        boolean czyJestemSpiacy;

        boolean czyOdebrac;


        for (int i = 0; i < tablicaZapytan.length; i++) {


            tablicaTablic[i] = tablicaZapytan[i].replaceAll(",", "").split(" ");

            czyJestRano = Boolean.valueOf(tablicaTablic[i][0]);
            czyDzwoniMama = Boolean.valueOf(tablicaTablic[i][1]);
            czyJestemSpiacy = Boolean.valueOf(tablicaTablic[i][2]);


            if (!czyJestRano) {

                czyOdebrac = true;
                System.out.println(czyOdebrac);

            } else if (czyDzwoniMama) {

                if (!czyJestemSpiacy) {

                    czyOdebrac = true;
                    System.out.println(czyOdebrac);

                } else {

                    czyOdebrac = false;
                    System.out.println(czyOdebrac);
                }

            } else {

                czyOdebrac = false;
                System.out.println(czyOdebrac);
            }
        }
    }
}



